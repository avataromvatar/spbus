
import 'package:Cores/Cores.dart';
import 'package:flutter_test/flutter_test.dart';

void main()
{
  Resources res = Resources();
  Resources res1 = Resources();
  res['test1'] = 10;
  res['test2'] = 20;
  res1['test2'] = 33;
  res1['test4'] = 40;
  
  Resources res2 = res.copy();
  Resources res3 = res1.copy();

  test("Test add", (){
    res2.add(res3);
    expect(res2.contains("test4"), true);
    expect(res3.contains("test1"), false);
  });
  test("Test sub", (){
    res2.sub(res3);
    expect(res2.contains("test2"), false);
  });
  test("Test coincidence", (){
    res.coincidence(res1,(String key, dynamic obj1, dynamic obj2){
      return obj1+obj2;
    });
    expect(res["test2"], 53);
  });
  test("Test difference", (){
    res.difference(res1,(String key, dynamic fromThis, dynamic fromRes){
      if(fromThis!=null)
      {
        res1[key]=fromThis;
      }
      if(fromRes!=null)
      {
        res[key]=fromThis;
      }
      return true;
    });
    expect(res.contains("test4"), res1.contains("test1"));
  });

}
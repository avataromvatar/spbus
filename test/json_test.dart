import 'dart:convert';

import 'package:Cores/Cores.dart';
import 'package:flutter_test/flutter_test.dart';


void main() {
  Core c1 = Core("Test");
  c1.resources["val1"] = 10;
  c1.resources["list"] = [10, 11, 12, 13];
  c1.resources["map"] = {"1": 20, "2": 22};
  c1.resources["string"] = "STRING";
  Core c2 = Core("Test2");
  c2.resources["map"] = {"1": 30, "2": 32};
  c1.resources["c2"] = c2;
  String jsonStr = jsonEncode(c1.toJson());
  Map<String, dynamic> json = jsonDecode(jsonStr);
  // , reviver: (Object o1, Object o2) {
  //   print(o2.runtimeType.toString());
  //   if (o2.runtimeType.toString() == "Map") {
  //     print(o2);
  //   }

  //   return jsonDecode(o2);
  // });
  var tmp = Core.fromJson(json);
  ICore ctmp = tmp.resources["c2"];
  print('${tmp.name} - ${ctmp.name}');
  test("Test Core toJson and fromJson", () {
    expect(tmp.name, "Test");
    expect(ctmp.name, "Test2");
  });
}



import 'package:Cores/Cores.dart';
import 'package:Cores/SimpleBuses.dart';
import 'package:flutter_test/flutter_test.dart';

import 'dart:async';

/*
  event "Add" <int> in 3 thread 

*/

// int test = 0;
Future<void> main() async {
  Core c1 = Core("c1");
  Core c2 = Core("c2");
  Core c3 = Core("c3");
  Core c4 = Core("c4");
  Core c5 = Core("c5");
  Core c6 = Core("c6");
  Core c7 = Core("c7");
  Core c8 = Core("c8");
  c1.setDebug(true); c1.serialBus.setMaxCountToTransportObjeect(100);
  c2.setDebug(true); c2.serialBus.setMaxCountToTransportObjeect(100);
  c3.setDebug(true); c3.serialBus.setMaxCountToTransportObjeect(100);
  c4.setDebug(true); c4.serialBus.setMaxCountToTransportObjeect(100);
  c5.setDebug(true); c5.serialBus.setMaxCountToTransportObjeect(100);
  c6.setDebug(true);
  c7.setDebug(true);
  c8.setDebug(true);
  

  c1.parallelBus.addTopic("MathUP");
  c1.parallelBus.addTopic("MathDown");
  c1.parallelBus.listen("MathUP", (data) {
    c2.serialBus.sendUP(data);
    c4.serialBus.sendUP(data);
    c5.serialBus.sendUP(data);
    c8.serialBus.upStream.receive(data); //Отработает и выполнит правило sendFurther
  });
  c1.parallelBus.listen("MathDown", (data) {
    c2.serialBus.downStream.receive(data); //Отработает и выполнит правило sendFurther
    c4.serialBus.sendDown(data);
    c5.serialBus.sendDown(data);
    c8.serialBus.sendDown(data);
  });
  c3.serialBus.connect(c2.serialBus);
  c4.serialBus.connect(c3.serialBus);
  c8.serialBus.connect(c7.serialBus);
  c7.serialBus.connect(c6.serialBus);
  c6.serialBus.connect(c5.serialBus);

  c2.serialBus.addRulesToDownStream("MathDown", ProcessingRules<List<int>>.print());
  c2.serialBus.addRulesToDownStream("MathDown", ProcessingRules<List<int>>("sum", sum)); //3 Так как на с2 вызывается recieve
  c2.serialBus.addRulesToDownStream("MathDown", ProcessingRules<List<int>>.sendFurther());
  c3.serialBus.addRulesToDownStream("MathDown", ProcessingRules<List<int>>("sum", sum)); //6
  c3.serialBus.addRulesToDownStream("MathDown", ProcessingRules<List<int>>.sendFurther());
  c4.serialBus.addRulesToDownStream("MathDown", ProcessingRules<List<int>>("sum", sum)); //9
  c4.serialBus.addRulesToDownStream("MathDown", ProcessingRules<List<int>>.sendFurther());

  c8.serialBus.addRulesToUpStream("MathUP", ProcessingRules<List<int>>.print());
  c8.serialBus.addRulesToUpStream("MathUP", ProcessingRules<List<int>>("sum", sum)); //5 Так как на с8 вызывается recieve
  c8.serialBus.addRulesToUpStream("MathUP", ProcessingRules<List<int>>.sendFurther());
  c7.serialBus.addRulesToUpStream("MathUP", ProcessingRules<List<int>>("sum", sum)); //10
  c7.serialBus.addRulesToUpStream("MathUP", ProcessingRules<List<int>>.sendFurther());
  c6.serialBus.addRulesToUpStream("MathUP", ProcessingRules<List<int>>("sum", sum)); //15
  c6.serialBus.addRulesToUpStream("MathUP", ProcessingRules<List<int>>.sendFurther());
  c5.serialBus.addRulesToUpStream("MathUP", ProcessingRules<List<int>>("sum", sum)); //20
  c5.serialBus.addRulesToUpStream("MathUP", ProcessingRules<List<int>>.sendFurther());
  TransportObject<List<int>> to1 = TransportObject<List<int>>("MathDown", [1, 2, 0], 0, "main1");
  TransportObject<List<int>> to2 = TransportObject<List<int>>("MathUP", [2, 3, 0], 0, "main2");

  c4.insertBetween(c2, c3);
  await fetchUserOrder();
  c6=null;

  c1.parallelBus.send(to1);
  c1.parallelBus.send(to2);

  await fetchUserOrder();

  test("Test Down and Up Stream", () {
    print("Test DOWNSTREAM");
    expect(to1.obj[2], equals(291));
    print("Test UPSTREAM");
    expect(to2.obj[2], equals(20));
  });
  // if (to1.obj[2] == 9 && to2.obj[2] == 20) {
  //   print("TEST OK");
  // } else {
  //   print("TEST Error");
  // }

  // var i =test('Math Test', () async {
  //   c1.sendEvent(to1);
  //   c1.sendEvent(to2);
  //   var v = await fetchUserOrder();
  //   expect(to1.obj[2], equals(9));
  //   expect(to2.obj[2], equals(15));
  // });
}

bool sum(TransportObject<List<int>> obj, {IPort transmiter, IPort receiver}) {
  String str_tr;
  String str_re;
  if (transmiter != null)
    str_tr = transmiter.name;
  else
    str_tr = "undefined";
  if (receiver != null)
    str_re = receiver.name;
  else
    str_re = "undefined";
  print('Port $str_re. Take data from $str_tr. Execute Rules SUM:${obj.obj[2]}');
  obj.obj[2] += obj.obj[0] + obj.obj[1];
  print("${obj.obj[0]} + ${obj.obj[1]} =+ ${obj.obj[2]}");
  return true;
}

Future<void> fetchUserOrder() {
  // Imagine that this function is fetching user info from another service or database.
  return Future.delayed(Duration(seconds: 2), () {
    print("Time check Test!");
  });
}

part of SimpleBuses;

// import 'package:SimpleBuses/src/TransportObject/TransportObject.dart';

// import 'dart:async';

// import 'package:SimpleBuses/src/Listener/Listeners.dart';
// import 'package:SimpleBuses/src/Resources/Resources.dart';
// import 'package:SimpleBuses/src/Rules/IProcessingRules.dart';
// import 'package:SimpleBuses/src/TransportObject/TransportObject.dart';

// import 'IPorts.dart';

// class __TransportObject<OBJECT> {
//   TransportObject<OBJECT> tO;
//   IPort sender;
//   String nameSender;
//   _TopicStream topic;
//   __TransportObject(this.tO, this.sender, this.nameSender, this.topic);
// }

class _TopicStream<T> {
  Type _typeObj;

  ///type of OBJECT
  Type get typeObj => _typeObj;
  List<IProcessingRules<T>> rules;
  List<Listener<T>> listener;

  _TopicStream() {
    _typeObj = T.runtimeType;
    rules = List<IProcessingRules<T>>();
    listener = List<Listener<T>>();
  }
  _TopicStream<T> clone()
  {
      _TopicStream<T> ret = _TopicStream<T>();
      ret._typeObj = T.runtimeType;
      if(rules.length>0)
      ret.rules = rules.sublist(0);
      if(listener.length>0)
      ret.listener = listener.sublist(0);
      return ret;
  }
}

class Port implements IPort {
  int maxTransportObjectCOUNT = 100;
  String _name;
  String get name => _name;
  String _path;
  String get path => _path;
  bool debugOn = false;
  bool unknownTopicSendFurther = false;
  IProcessingRules unknownTopicProcessingRules;
  List<IPort> _receivers;

  ///Добавьте себя если вы связываетесь с этим портом что бы он отсоеденил себя при удалении
  ///Тут те кто к тебе подключился( у кого ты в _receivers)
  Set<IPort> _link;

  Map<String, _TopicStream<dynamic>> _topic;

  ///TransportObject List [0] TransportObject<OBJECT> [1] sender
  StreamController<_TransportObject<dynamic>> _controller;
  StreamSubscription _subscription;

  Port(this._name, {String path = "/", bool unknownTopicSendFurther = true, bool debugOn = false, int maxTransportObjectCOUNT = 100}) {
    _link = Set<IPort>();
    _path = "$path$_name";
    _receivers = List<IPort>();
    _topic = Map<String, _TopicStream<dynamic>>();
    _controller = StreamController<_TransportObject<dynamic>>();
    _subscription = _controller.stream.listen(_transmitMSG);

    if(unknownTopicSendFurther != null)
    this.unknownTopicSendFurther = unknownTopicSendFurther;
    else 
    this.unknownTopicSendFurther = true;
    if(debugOn != null)
    this.debugOn = debugOn;
    else
    this.debugOn = false;

  }
  void changePath(String newPath) {
    _path = "$newPath$name";
  }

  Port.clone(this._name, Port port, {String path = "/", bool unknownTopicActionClone = true,bool cloneRules=true}) {
    _link = Set<IPort>();
    _path = "$path$name";
    _receivers = List<IPort>();
    _topic = Map<String, _TopicStream<dynamic>>();
    _controller = StreamController<_TransportObject<dynamic>>();
    _subscription = _controller.stream.listen(_transmitMSG);
    maxTransportObjectCOUNT = port.maxTransportObjectCOUNT;
    if (unknownTopicActionClone) {
      unknownTopicSendFurther = port.unknownTopicSendFurther;
      unknownTopicProcessingRules = port.unknownTopicProcessingRules;
    }
    port._receivers.forEach((element) {
      connect(element);
    });
    port._link.forEach((element) {
      element.connect(this);
    });
    if(cloneRules)
    port._topic.forEach((key, value) {
      _topic[key] = value.clone();
    });
  }

  void _transmitMSG(_TransportObject<dynamic> tO) {
    bool flag = false;
    if (_topic.containsKey(tO.tO.topic)) {
      var top = _topic[tO.tO.topic];
      // if(top.listener.length>0)
      //   flag = true;
      top.listener.forEach((element) {
        element.notification(tO.tO);
      });
    }
    var tmp1 = _receivers.sublist(0, _receivers.length);
    if (tmp1.length > 0) {
      tmp1.forEach((element) {
        element.receive(tmp1.length > 1 ? tO.tO.clone(path: path, to: element.path) : tO.tO, sender: tO.sender);
      });
      if (tmp1.length > 1) tO.tO.itsShared(path: path);
    } else {
      if (!flag) tO.tO.itsNoHaveReceivers(path: path);
    }
  }

  ///Оповещает слущающих о том что port закрывается и отписывается от всех
  Future<void> toDelete() async {
    if (debugOn) print("Port $_path delete begin");
    await _subscription.cancel();
    await _controller.close();
    _topic.forEach((key, value) {
      value.listener.forEach((element) {
        if (element.listenerStatus.topicDeleteCallback != null) element.listenerStatus.topicDeleteCallback(element.listenerStatus);
        element.listenerStatus.cancel();
      });
      value.listener.clear();
      value.rules.clear();
    });
    _receivers.forEach((element) {
      element.disconnect(this);
    });
    _receivers.clear();
    if (debugOn) print("Port $_path delete END");
  }

  void addTopic<T>(String name) {
    if (!_topic.containsKey(name)) {
      if (debugOn) print("Port $_path add topic $name");
      _topic[name] = _TopicStream<T>();
    }
  }

  void removeTopic(String name) {
    if (_topic.containsKey(name)) {
      if (debugOn) print("Port $_path remove topic $name");
      _topic[name].listener.forEach((element) {
        if (element.listenerStatus.topicDeleteCallback != null) element.listenerStatus.topicDeleteCallback(element.listenerStatus);
      });
      _topic.remove(name);
    }
  }

  bool send<T>(TransportObject<T> transportObject, {IPort sender, String senderName}) {
    if(!transportObject.isEnded){
    transportObject.count++;
    if(transportObject.count<maxTransportObjectCOUNT)
    {
    _controller.add(new _TransportObject(transportObject, sender!=null?sender:this, transportObject.topic, senderName==null?this.path:senderName));
    if (debugOn) print("Data from $senderName send to $_path->${transportObject.topic}");
    
    return true;
    }
    transportObject.itsRecursive(path: path);
    //throw Exception("${sender.name} send ${transportObject.topic} from creater ${transportObject.creatorName} but count ${transportObject.count} > ${maxTransportObjectCOUNT}");
    if (debugOn) print("ALERT!! ${sender.name} send ${transportObject.topic} from creater:${transportObject.creatorName}, but count ${transportObject.count} > ${maxTransportObjectCOUNT}");

    }
    return false;
  }

  bool addRule<T>(String topic, IProcessingRules<T> rules) {
    addTopic<T>(topic);
    var top = _topic[topic];
    if (top.typeObj == T.runtimeType) {
      if (debugOn) print("Port $_path topic $topic. Add Rules ${rules.name} - OK");
      top.rules.add(rules);
      return true;
    }
    if (debugOn) print("Port $_path topic $topic. Add Rules ${rules.name} - ERROR TYPE TRANSPORT OBJ");
    return false;
  }

  void removeRule<T>(String topic, {IProcessingRules<T> rules}) {
    if (_topic.containsKey(topic)) {
      if(rules!=null){
      if (debugOn) print("Port $_path Remove Rules ${rules.name} - OK");
      _topic[topic].rules.remove(rules);
      }
      else
      {
         if (debugOn) print("Port $_path Remove All Rules OK");
         _topic[topic].rules.clear();
      }
    }
  }

  void addLinkToDisconnect(IPort link) {
    _link.add(link);
  }

  void removeLinkToDisconnect(IPort link) {
    _link.remove(link);
  }

  void connect(IPort port) {
    if (debugOn) print("Port $_path Connect to ${port.path}");
    port.addLinkToDisconnect(this);
    _receivers.add(port);
  }

  void disconnect(IPort port) {
    if (_receivers.contains(port)) {
      if (debugOn) print("Port $_path disconnect ${port.path}");
      _receivers[_receivers.indexOf(port)].removeLinkToDisconnect(this);
      _receivers.remove(port);
    }
  }

  void disconnectAll() {
    var tmp = _link.take(_link.length);
    tmp.forEach((element) {
      element.disconnect(this);
    });
    _receivers.forEach((element) {
      element.removeLinkToDisconnect(this);
    });
    _receivers.clear();
  }

  List<IProcessingRules<dynamic>> getRules(String nameTopic) {
    if (_topic.containsKey(nameTopic)) {
      return _topic[nameTopic].rules;
    }
    return null;
  }

  ListenerStatus<T> listen<T>(String topic, Function(TransportObject<T> data) func, {List<int> waitState}) {
    if (_topic.containsKey(topic)) {
      var l = Listener<T>(func, (Listener<T> listener) {
        _removeListener(topic, listener);
      }, waitState);
      _topic[topic].listener.add(l);
      if (debugOn) print("Port $_path Add Listener ${l.hashCode} - OK");
      return l.listenerStatus;
    }
    return null;
  }

  void softRemoveAllListeners() {
    if (debugOn) print("$_path soft Remove Listener");
    _topic.forEach((key, value) {
      value.listener.clear();
    });
  }

  void hardRemoveAllListeners() {
    if (debugOn) print("$_path hard Remove Listener");
    _topic.forEach((key, value) {
      value.listener.forEach((element) {
        if (element.listenerStatus.topicDeleteCallback != null) element.listenerStatus.topicDeleteCallback(element.listenerStatus);
        if (debugOn) print("$_path Remove Listener ${element.hashCode} from $key - OK");
      });
      value.listener.clear();
    });
  }

  void _removeListener<T>(String topic, Listener<T> listener) {
    if (_topic.containsKey(topic)) {
      if (_topic[topic].listener.remove(listener)) {
        if (debugOn) print(" $_path Remove Listener ${listener.hashCode} from $topic - OK");
        return;
      }
    }
    if (debugOn) print("$_path Remove Listener ${listener.hashCode} from $topic - WARNING NO LISTENER");
  }

  void receive<T>(TransportObject<T> transportObject, {IPort sender, String senderName}) {
    //В начале оповещаем слущающих
    if (_topic.containsKey(transportObject.topic)) {
      var top = _topic[transportObject.topic];
      // top.listener.forEach((element) {
      //   element.notification(transportObject);
      // });
      //Отрабатываем цепочку правил
      top.rules.forEach((element) {
        if (!transportObject.isEnded) {
          if (debugOn) print("$_path->${transportObject.topic} run rules ${element.name}");
          if (!element.run(transportObject, transmiter: sender, receiver: this)) {
            transportObject.itsBroken(path: path);
          }
        }
      });
    } else if (unknownTopicSendFurther) {
      send(transportObject, sender: sender, senderName: senderName);
    } else if (unknownTopicProcessingRules != null) {
      unknownTopicProcessingRules.run(transportObject, transmiter: sender, receiver: this);
    } else
      transportObject.itsUnknown(path: path);
  }

  List<String> getTopic() {
    return _topic.keys.toList();
  }

  IPort clone({String name, bool unknownTopicActionClone = true, String path}) {
    if (name != null)
      return Port.clone(name, this, path: path != null ? path : this.path, unknownTopicActionClone: unknownTopicActionClone);
    else
      return Port.clone(this.name, this, path: path != null ? path : this.path, unknownTopicActionClone: unknownTopicActionClone);
  }

  void softRemove() {
    if (debugOn) print("$_path SOFT REMOVE");
    _link.forEach((element) {
      _receivers.forEach((element1) {
        element.connect(element1);
      });
    });
    disconnectAll();
  }

  void copyTo(IPort to) {
    if (debugOn) print("$_path COPY TO ${to.path}");
    to.disconnectAll();
    _receivers.forEach((element) {
      to.connect(element);
    });
    _link.forEach((element) {
      to.addLinkToDisconnect(element);
    });
    //add topic and rules and listeners
    var tmp = to as Port;
    _topic.forEach((key, value) {
      to.addTopic(key);
      value.rules.forEach((element) {
        to.addRule(key, element);
      });
      tmp._topic[key].listener.addAll(value.listener);
    });
  }
}

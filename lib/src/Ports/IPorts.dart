part of SimpleBuses;
// import 'package:SimpleBuses/src/Listener/Listeners.dart';
// import 'package:SimpleBuses/src/Resources/Resources.dart';
// import 'package:SimpleBuses/src/Rules/IProcessingRules.dart';
// import 'package:SimpleBuses/src/TransportObject/TransportObject.dart';

enum ePortError { NO_ERROR, WRONG_TYPE_OF_OBJECT, UNDEFINED }

///Основной интерфейс обеспечивающий взаимодействия между узлами. 
///Обеспечивает последовательную и паралельную событийную шину.
///
///Когда отправляются новые данные с помощью [send] все 
///те кто был [connect], а это reciever, получают эти данные 
///с помощью вызова их метода [receive]
///
///Когда вызывается метод [connect] to XX(IPort) он добавляет в reciver 
/// XX с которым соединяемся, а себя записывает в XX.link(addLinkToDisconnect) 
///Таким образом мы всегда знаем кто с нами соединен, это нужно для того 
///чтобы при удалении или softRemove можно было убрать узел без критических 
///ошибок со стороны подсоединенных к нему  
///```
///Пример для upstream 
///P-IPort R - Port._reciever L -Port._link  
///         P1 R-0 L-P2
///         |
///         P2  R-P1 L-P4,P3
///       /     \
///       |     |
///       P4    P3
/// R-P2 L-0    R-P2 L-0
/// В этом примере if P4.send, то будут вызваны:
/// > P2.reciver если у него будет правило 
/// ProcessingRules.sendFurther, то
/// >> P2.send который вызовет
/// >>> P1.reciver 
///  
/// Если вызвать P2.softRemove то получется следующее:
///         P1  R-0 L-P3,P4
///        /  \
///       P4   P3
///     R-P1    R-P1
/// Если вызвать P2.disconnectAll то получется следующее:
///         P1 R-0 L-0
///         
///       P4    P3
/// R-0 L-0    R-0 L-0
/// Если вызвать P2.disconnect(P1) то получется следующее:
///         P1 R-0 L-0
///         
///         P2  R-0 L-P4,P3
///       /     \
///       |     |
///       P4    P3
/// R-P2 L-0    R-P2 L-0
/// Если вызвать P2.disconnect(P3) то ничего неизменится 
/// так как P2 не подключен к P3
/// ```
abstract class IPort {
  /// Имя порта
   String get name;
  /// Путь к порту
   String get path;
  ///Ограничение пересылок одного и того же сообщения
  ///Защита от зацикливания.
  int maxTransportObjectCOUNT = 100;
  ///need print debug info if true
  bool debugOn = false;
  ///Изменяет путь к порту
  void changePath(String newPath);
  ///Если у нас нет такого топика отправить дальше если true
  bool unknownTopicSendFurther = false;
  ///Если у вас нету данного топика вызовится данный метод
  ///if unknownTopicProcessingRules != null
  IProcessingRules unknownTopicProcessingRules;
  ///отмечает Порт к удалению
  Future<void> toDelete();

  /// add topic for send and receive
  void addTopic<T>(String name);

  ///Remove topic
  void removeTopic(String name);

  ///Получить список всех топиков
  List<String> getTopic();

  /// notificate listeners who wait topic and call [IPort.receive] to each connection
  /// return false if count transportObject > maxTransportObjectCOUNT or isEnded
  bool send<T>(TransportObject<T> transportObject, {IPort sender, String senderName});

  ///receive [transportObject] and execute [IProcessingRules] if IPort have topic
  ///if IPort not have this topic and  [unknownTopicSendFurther] set true then be called send method
  ///if [unknownTopicSendFurther] =false and  [unknownTopicProcessingRules] !=null be called [unknownTopicProcessingRules]
  void receive<T>(TransportObject<T> transportObject, {IPort sender, String senderName});

  ///add [IProcessingRules] to rules chain for execute.
  ///If port not have topic, this method create topic
  void addRule<T>(String topic, IProcessingRules<T> rule);

  ///remove all rules by [topic] or just [IProcessingRules] if set
  void removeRule<T>(String topic, {IProcessingRules<T> rules});
  ///get List rules for topic
  List<IProcessingRules<dynamic>> getRules(String nameTopic);

  ///Connect [port] to [this] (add [port] to receive)
  void connect(IPort port);

  ///Disconect [port] from receive
  void disconnect(IPort port);

  ///Disconect from all receive and all linkers
  void disconnectAll();
  /// **ВАЖНО** убирает всех подписчиков не сообщая им об этом,
  /// поэтому сделайте копию подписчиков перед тем как вызвать этот метод
  void softRemoveAllListeners();
  ///Убирает всех подписчиков оповещая их об этом call ListenerStatus.topicDeleteCallback
  void hardRemoveAllListeners();
  

  ///Listen receive event
  ListenerStatus<T> listen<T>(String topic, Function(TransportObject<T> data) func, {List<int> waitState});

  ///Этот линк необходим при разрушения порта.
  ///Когда вызывается функция [toDelete] у всех links вызывается метод [disconnect(this)]
  void addLinkToDisconnect(IPort link);

  ///Уберает линк при вызове disconnect
  ///Когда вызывается функция [toDelete] у всех links вызывается метод [disconnect(this)]
  void removeLinkToDisconnect(IPort link);
  ///Создает клонна данного порта с новым именем
  IPort clone({String name,bool unknownTopicActionClone=true,String path});
  ///Убирает себя из связи между узлами, сединяя узлы ниже и выше текущего, между собой
  ///Этот метод так же отсоединяет себя от всех узлов 
  void softRemove();
  ///Копируем this to [port] со всеми связями
  ///
  /// **ВАЖНО** [to] be disconectedAll() before copy
  void copyTo(IPort to);
}

part of SimpleBuses;
// import 'package:SimpleBuses/src/Ports/IPorts.dart';
// import 'package:SimpleBuses/src/Rules/IProcessingRules.dart';
// import 'package:SimpleBuses/src/TransportObject/TransportObject.dart';

class ProcessingRules<OBJECT> implements IProcessingRules<OBJECT> {
  String name;
  dynamic _class_tmp;
  // String event;
  bool Function(TransportObject<OBJECT> obj, {IPort transmiter, IPort receiver}) _execute;

  ProcessingRules(this.name, this._execute /*,{this.event=null}*/);
  bool run(TransportObject<OBJECT> obj, {IPort transmiter, IPort receiver}) {
    // if (event != null) {
    //   if (event == obj.name) {
    //     return _execute(obj, transmiter: transmiter, receiver: receiver);
    //   }
    // } else
    return _execute(obj, transmiter: transmiter, receiver: receiver);

    // return false;
  }

  ///Send further (дальше)
  ProcessingRules.sendFurther() {
    name = "sendFurther";
    _execute = (TransportObject<OBJECT> obj, {IPort transmiter, IPort receiver}) {
      receiver.send(obj, sender: receiver, senderName: receiver.name);
      return true;
    };
  }
  ProcessingRules.print() {
    name = "print";
    _execute = (TransportObject<OBJECT> obj, {IPort transmiter, IPort receiver}) {
      String str_tr;
      String str_re;
      if (transmiter != null)
        str_tr = transmiter.name;
      else
        str_tr = "undefined";
      if (receiver != null)
        str_re = receiver.name;
      else
        str_re = "undefined";
      print("Port $str_re get msg topic ${obj.topic} from $str_tr");
      return true;
    };
  }

  ProcessingRules.listen(Function(TransportObject<OBJECT> obj) func) {
    name = "listen";
    _class_tmp = func;
    _execute = (TransportObject<OBJECT> obj, {IPort transmiter, IPort receiver}) {
      _class_tmp(obj);
      return true;
    };
  }

  ProcessingRules.counter(Function() callback) {
    _class_tmp = callback;
    name = "counter";
    _execute = (TransportObject<OBJECT> obj, {IPort transmiter, IPort receiver}) {
      _class_tmp();
      return true;
    };
  }
  ProcessingRules.forwardMsg(IPort sendTo) {
    name = "forwardMsg";
    _execute = (TransportObject<OBJECT> obj, {IPort transmiter, IPort receiver}) {
      return sendTo.send(obj, sender: receiver, senderName: receiver.name);
    };
  }
}

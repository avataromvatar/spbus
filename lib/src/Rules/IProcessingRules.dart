// import 'package:SimpleBuses/src/Ports/IPorts.dart';
part of SimpleBuses;

// import 'package:SimpleBuses/src/TransportObject/TransportObject.dart';

abstract class IProcessingRules<OBJECT> {
  String name;
  // String event;
  ///if return false rules chain stop execute
  bool run(TransportObject<OBJECT> obj, {IPort transmiter, IPort receiver});
}

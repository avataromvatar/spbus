part of SimpleBuses;
// import 'package:SimpleBuses/src/TransportObject/TransportObject.dart';

class Listener<T> {
  ///Функция обратного вызова
  Function(TransportObject<T> data) func;

  ///Сервисный класс. позволяет удалить потом подиску
  ListenerStatus<T> listenerStatus;

  ///Список тех состояний которые будут допущены к обработчику
  List<int> arrState;

  Listener(this.func, Function(Listener<T>) funcRemove, List<int> state) {
    listenerStatus = ListenerStatus<T>(this, funcRemove);
    this.arrState = state;
  }

  /// Проверить есть ли состояни
  void notification(TransportObject<T> obj) {
    if (arrState != null) {
      if (arrState.length > 0) {
        if (arrState.contains(obj.state)) if (func != null) func(obj);
      }
    } else {
      if (func != null) func(obj);
    }
  }
}

/// Control subscribe for port event
class ListenerStatus<T> {
  ListenerStatus(this._listener, this._listenerRemove) {
    _status = 1;
  }
  Function(Listener<T>) _listenerRemove;
  Listener<T> _listener;
  Listener<T> get listener => _listener;

  /// Callback if port delete
  Function(ListenerStatus<T> del) topicDeleteCallback;

  ///count ++ then call listener
  int count = 0;

  int _status = 0;

  ///if 0 listener delete if 1 listener connect
  int get status => _status;

  ///Remove Listener
  void cancel() {
    if (_listener != null) _listenerRemove(_listener);
    _listener = null;
    _status = 0;
  }
}

part of Cores;

class RootCore extends Core implements IRootCore {
  Map<String, ICore> _allCores;
  Map<String, ICore> get allCores => _allCores;
  Map<int, Completer<void>> _compliters;
  // Map<String,List<Core>> _allLevels;
  // Map<String,List<Core>> get allLevels => _allLevels;

  RootCore(String name,
      {String className = "RootCore",
      String path = "",
      bool debugOn = false,
      bool unknownTopicSendFurther = true})
      : super(name,
            className: className,
            path: path,
            debugOn: debugOn,
            unknownTopicSendFurther: unknownTopicSendFurther) {
    _allCores = Map<String, ICore>();
    _compliters = Map<int, Completer<void>>();
  }

 static String clearPath(String path,{String delThisTo,String endSymbol=""})
 {
   String str="";
   var arr = path.split("/");
   if(arr!=null)
   {
   arr.remove("downstream");
    arr.remove("upstream");
    arr.remove("serialBus");
    arr.remove("parallelBus");
    if(delThisTo!=null)
    arr.remove(delThisTo);
    int count =0;
    arr.forEach((element) {
      if(element.length>0)
      {
      if(count ==0)
      str ="$element";
      else
      str =str+ "$element";
      count++;
      }
    });
    
    str =str+ endSymbol;
  return str;
   }
   return "";
 } 
ICore getCore(String path)
{
  String str = clearPath(path);
  if(_allCores.containsKey(str))
  {
    return _allCores[str];
  }
  return null;
}


 ///Ищет по [path] ядра отсекая downStream|upStream|serialBus|parallelbus
 bool contains(String path)
 {
   return _allCores.containsKey(clearPath(path));
 }

  Future<void> buildTree() {
    final cmp = Completer<void>();
    _compliters[cmp.hashCode]=cmp;
    _allCores.clear();
    var tmp = Resources();
    tmp["RootPath"] = _path;
    tmp["AllCores"] = _allCores;
    serialBus.sendDown(TransportObject("__ping__", tmp, 0, path,
        identificator: cmp.hashCode, inTheEnd: _pingEnd));
    return cmp.future;
  }

  void _pingEnd(TransportObject objEnd, int codeOfEnd,
      {String path, int cloneCount}) {
    if (cloneCount == 0) {
      if (_compliters.length > 0) {
        if (_compliters.containsKey(objEnd.identificator)) {
          _compliters[objEnd.identificator].complete();
          _compliters.remove(objEnd.identificator);
        }
      }
    }
  }
}

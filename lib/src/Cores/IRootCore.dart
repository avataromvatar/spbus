part of Cores;

abstract class IRootCore
{
  ///all Core in Root
  ///String - path, ICore - ICore  
  Map<String,ICore> get allCores;
 
 ///Получить ядро или null если его нет
 ICore getCore(String path); 
 ///Ищет по [path] ядра отсекая downStream|upStream|serialBus|parallelbus
 bool contains(String path);
 ///Пока только строит allCores 
 ///до полноценного дерева далеко 
 Future<void> buildTree();
}
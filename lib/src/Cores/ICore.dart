part of Cores;

abstract class ICore  {
  // String _name;
  String get path;
  String get name;
  String get className;
  // Resources _resources;
  ///Ресурсы узла
  Resources get resources;
  SerialBus get serialBus;
  ParallelBus get parallelBus; 

  ///Изменяет путь к ядру
  void changePath(String newPath);
  ///Подготавливает узел к удалению
  ///уничтожает все связи и все темы
  Future<void> toDelete();
  ///Подключает к друг другу узлы нижние и верхнии
  ///затем отсоединяет себя от всех узлов
  void softRemove();
  ///Вставляет себя между узлами
  ///Результат:
  ///```
  ///upcore --- this --- downcore
  ///```
  ///### ВАЖНО
  ///все соединеия делаются с низу вверх,
  ///тоесть upcore выше чем downcore 
  bool insertBetween(ICore upcore,ICore downcore);

}

part of Cores;
// import 'package:SimpleBuses/src/Cores/ICore.dart';
// import 'package:SimpleBuses/src/ParallelBus/ParallelBus.dart';
// import 'package:SimpleBuses/src/Resources/Resources.dart';
// import 'package:SimpleBuses/src/SerialBus/SerialBus.dart';

class Core implements ICore {
  String _path;
  String get path => _path;
  String _name;
  String get name => _name;
  String _className;
  String get className => _className;

  Resources _resources;
  Resources get resources => _resources;
  SerialBus _serialBus;
  SerialBus get serialBus =>_serialBus;
  ParallelBus _parallelBus;
  ParallelBus get parallelBus =>_parallelBus;
  bool debugOn;
  
  Core(String name,{String className="Core",String path="",this.debugOn=false,bool unknownTopicSendFurther = true}) {

    _name = name;
    _className = className;
    _path = "$path$name/";
    _resources = Resources();
    _serialBus = SerialBus("serialBus",path:"${_path}",unknownTopicSendFurther:unknownTopicSendFurther,debugOn: debugOn);
    _parallelBus = ParallelBus("parallelBus",path:"${_path}",debugOn: debugOn);
     
     _serialBus.addRulesToDownStream("__ping__", ProcessingRules.listen(_ping));
     _serialBus.addRulesToDownStream("__ping__",ProcessingRules.sendFurther());
    // setDebug(debugOn);
  }
  Core.clone(Core clone,{String name,bool unknownTopicActionClone=true,String path,this.debugOn=false,bool cloneRules=true}) {
    if(name!=null)
      _name = name;
    else
    _name = clone.name;
  if(path!=null) // _path = "$path$name/";
    _path = "$path$_name/";
    else
    _path = "${RootCore.clearPath(clone.path,delThisTo:clone.name,endSymbol: "")}$_name/";

    _resources = clone.resources.copy();
    _serialBus = SerialBus.clone(clone.serialBus,name:"serialBus",path:"${_path}",cloneRules:cloneRules);
    _serialBus.downStream.removeRule("__ping__");
    _serialBus.addRulesToDownStream("__ping__", ProcessingRules.listen(_ping));
     _serialBus.addRulesToDownStream("__ping__",ProcessingRules.sendFurther());

    _parallelBus = ParallelBus.clone(clone.parallelBus,name:"parallelBus", path:"$_path");
  }

bool _ping(TransportObject<dynamic> obj)
{
  String p = obj.obj["RootPath"];
       Map<String, ICore> tmp = obj.obj["AllCores"];
      //  changePath("$p$_path");
       String str = RootCore.clearPath(_path);
       if(!tmp.containsKey(str))
        tmp[str] = this;
       else{
          if(debugOn)
            print("WARNING: $_path CANT ADD TO RootCore this path not unique!!");
       }
      //  else
        // for(int i=1;i<1000;i++)
        // {
        //   str = "$_path$i";
        //   if(!tmp.containsKey(str))
        //   {
        //     tmp[str] = this;
        //     if(debugOn)
        //     print("$_path CANT ADD TO RootCore this path not unique. Add $str to RootCore");
        //     return true;
        //   }  
        // } 
      return true;
}

void setDebug(bool val) {
  debugOn = val;
    _parallelBus.parallelPort.debugOn = val;
    _serialBus.downStream.debugOn = val;
    _serialBus.upStream.debugOn = val;
  }
  void changePath(String newPath)
  { 
    if(debugOn==true)
    {
      print("changePath $_path -> $newPath");
    }
    _path = newPath;
    _serialBus.changePath(newPath);
    _parallelBus.changePath(newPath);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> ret = Map<String, dynamic>();
    ret['typeClass'] = _className;
    ret['name'] = _name;
    ret['resources'] = resources.toJson();
    return ret;
  }

  // Core.fromJson(Map<String, dynamic> json) {
  //   if (json.containsKey('typeClass')) {
  //     if (json['typeClass'] == "Core") {
  //       _name = json['name'];
  //       resources.fromJson(json['resources']);
  //     }
  //   }
  // }

  static ICore fromJson(Map<String, dynamic> json, {dynamic Function(String key, Map<String, dynamic> map) func = null}) {
    if (json.containsKey('typeClass')) {
      if (json['typeClass'] == "Core") {
        Core ret = Core(json['name']);
        ret.resources.fromJson(json['resources'], typeClassCB: func);
        return ret;
      }
    }
    return null;
  }

  Future<void> toDelete() async
  {
      await parallelBus.parallelPort.toDelete();
      await serialBus.downStream.toDelete();
      await serialBus.upStream.toDelete();
      resources.resources.clear();
  }
  void softRemove()
  {
      serialBus.softRemove();
  }


  
  bool insertBetween(ICore upcore,ICore downcore)
  {
    // core1.serialBus.disconnect(core2.serialBus);
    downcore.serialBus.disconnect(upcore.serialBus);
    serialBus.connect(upcore.serialBus);
    downcore.serialBus.connect(serialBus);

  }




  @override //overriding noSuchMethod
  noSuchMethod(Invocation invocation) {
    print('Got the ${invocation.memberName} with arguments ${invocation.positionalArguments}');
    return null;
  }
}

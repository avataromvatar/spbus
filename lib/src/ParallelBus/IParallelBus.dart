part of SimpleBuses;


///### ParallelBus
///---
///Событийная шина, где события представлены темами и состояниями [state]
///Состояние меняется от 0 - 0xFFFFFFFF. Вы можете слушать как все состояния
///так и какие-то определнные представленные в listen.[needState] 
abstract class IParallelBus {
  IPort get parallelPort;
  // bool init(String name);
  String get name ;
  ///Path to Port
  String get path;
  ///Parallel mechanics. get event topic list
  List<String> getTopicList();
  ///Изменяет путь к шине
  void changePath(String newPath);
  ///Закрывает все топики, вызывая removeTopic
  void clear();
  ///Parallel mechanics. Create event topic
  void addTopic<T>(String topic);

  ///Parallel mechanics. Remove event topic
  void removeTopic<T>(String topic);

  ///Parallel mechanics. subcribe to topic
  ListenerStatus<T> listen<T>(String topic, Function(TransportObject<T> data) func, {List<int> needState});

  ///Parallel mechanics. Send event to the bus
  bool send<T>(TransportObject<T> obj, {IPort sender, String senderName});

}

part of SimpleBuses;

// import 'dart:async';

// import 'package:SimpleBuses/src/Listener/Listeners.dart';
// import 'package:SimpleBuses/src/ParallelBus/IParallelBus.dart';
// import 'package:SimpleBuses/src/Ports/IPorts.dart';
// import 'package:SimpleBuses/src/Ports/Ports.dart';
// import 'package:SimpleBuses/src/TransportObject/TransportObject.dart';

class _TransportObject<T> {
  TransportObject<T> tO;
  IPort sender;
  String event;
  String senderName;
  _TransportObject(this.tO, this.sender, this.event, this.senderName);
}

class ParallelBus implements IParallelBus {
  // Map<String, IPort> _events;
  IPort _port;
  IPort get parallelPort => _port;
  String _name;
  String get name => _name;
  String _path;
  String get path =>_path;
  // StreamController<_TransportObject<dynamic>> _controller; //[0] TransportObject<OBJECT> [1] sender
  // StreamSubscription _subscription;
  bool _isInit = false;
  // bool get debugOn => _port.debugOn;
  // set debugOn(bool val) => _port.debugOn = val;
  ParallelBus(this._name,{String path = "/",bool debugOn=false})
  {
     _path = "$path$_name";
    _port = Port(_name,path:_path,debugOn: debugOn );
    // _port.debugOn = debugOn;
   
  }
  
  ParallelBus.cloneFromPort(IPort port,{bool debugOn=false,String name,bool unknownTopicActionClone=true,String path})
  {
    if(name!=null)
    {
      _port = port.clone(name:name,unknownTopicActionClone:unknownTopicActionClone,path: path!=null?path:RootCore.clearPath( port.path));
      _name = name;
    }
    else
    {
      _port = port.clone(name:port.name,unknownTopicActionClone:unknownTopicActionClone,path: path!=null?path:RootCore.clearPath( port.path));
      _name = port.name;
    }
    _port.debugOn = debugOn;
  }
  ParallelBus.clone(IParallelBus pbus,{bool debugOn=false,String name,bool unknownTopicActionClone=true,String path})
  {
    if(name!=null)
    {
      _name = name;
      _port = pbus.parallelPort.clone(name:name,unknownTopicActionClone:unknownTopicActionClone,path: path!=null?path:pbus.path );
    }
    else
    {
    _port = pbus.parallelPort.clone(name:pbus.name,unknownTopicActionClone:unknownTopicActionClone,path: path!=null?path:pbus.path);
     _name = pbus.name;
    }
  }
///Изменяет путь к шине
  void changePath(String newPath)
  {
    _path = "$newPath$_name";
  _port.changePath(newPath);
  }
void clear()
{
  var tmp = _port.getTopic();
  tmp.forEach((element) {
    _port.removeTopic(element);
  });
}

  ///Parallel mechanics. get event topic list
  List<String> getTopicList() {
    return _port.getTopic();
  }

  ///Parallel mechanics. Create event topic
  void addTopic<T>(String topic) {
    _port.addTopic(topic);
  }

  ///Parallel mechanics. Remove event topic
  void removeTopic<T>(String topic) {
    _port.removeTopic(topic);
  }

  ///Parallel mechanics. subcribe to topic
  ListenerStatus<T> listen<T>(String topic, Function(TransportObject<T> data) func, {List<int> needState}) {
    return _port.listen<T>(topic, func, waitState: needState);
  }

  ///Parallel mechanics. Send event to the bus
  bool send<T>(TransportObject<T> obj, {IPort sender, String senderName}) {
    return _port.send<T>(obj, sender: sender, senderName: senderName);
  }
}

part of Cores;
// import 'IResources.dart';

class Resources implements IResources {
  Map<String, dynamic> resources = Map<String, dynamic>();

  // Resources()
  // {
  //   resources=Map<String,ResourcesContainer>();
  // }
  Map<String, dynamic> toJson() => resources;

  void fromJson(Map<String, dynamic> json, {bool overwrite = true, dynamic Function(String key, Map<String, dynamic> map) typeClassCB = null}) {
    json.forEach((key, value) {
      if (!resources.containsKey(key) || overwrite) {
        // print(value.runtimeType.toString());
        if (value.runtimeType.toString() == "_InternalLinkedHashMap<String, dynamic>") {
          Map<String, dynamic> tmp = value as Map<String, dynamic>;
          if (tmp.containsKey("typeClass")) {
            if (typeClassCB != null)
              value = typeClassCB(key, tmp);
            else if (tmp['typeClass'] == "Core") {
              value = Core.fromJson(tmp);
            }
          }
        }
        resources[key] = value;
      }
    });
  }

  IResources copy()
  {
    Resources ret = Resources();
    resources.forEach((key, value) {
      ret[key] = value;
    });
    return ret;
  }

  void coincidence(IResources res, dynamic Function(String key, dynamic obj1, dynamic obj2) func) {
    res.resources.forEach((key, value) {
      if (resources.containsKey(key))
      {
        var tmp = func(key,resources[key],res[key]);
        if(tmp == null)
          return;
        resources[key] = tmp;
      }
    });
  }

  void difference(IResources res, bool Function(String key, dynamic fromThis, dynamic fromRes) func) {
    res.resources.forEach((key, value) {
      if (!resources.containsKey(key))
      {
        var tmp = func(key,null,res[key]);
        if(tmp == false)
          return;
      }
    });
    resources.forEach((key, value) {
      if (!res.contains(key))
      {
        var tmp = func(key,resources[key],null);
        if(tmp == false)
          return;
      }
    });
  }

  void sub(IResources res) {
    res.resources.forEach((key, value) {
      if (resources.containsKey(key)) {
        resources.remove(key);
      }
    });
  }

  void add(IResources res, {bool overwrite = false,dynamic Function(String key, dynamic obj1, dynamic obj2) coincidence}) {
    res.resources.forEach((key, value) {
      if (!resources.containsKey(key) || overwrite) {
        resources[key] = value;
      }
      else{
        if(coincidence !=null)
        {
          resources[key] = coincidence(key,resources[key],value);
        }
      }
    });
  }

  bool removeResource(String name) {
    if (resources.containsKey(name)) {
      resources.remove(name);
      return true;
    }
    return false;
  }

  bool addResource(String name, dynamic res) {
    if (resources.containsKey(name)) return false;
    resources[name] = res;
    return true;
  }

  bool contains(String name) {
    return resources.containsKey(name);
  }

  dynamic operator [](String name) {
    if (!resources.containsKey(name))
      return null;
    else
      return resources[name];
  }

  operator []=(String name, dynamic res) {
    resources[name] = res;
  }
}

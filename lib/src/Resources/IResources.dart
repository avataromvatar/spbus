part of Cores;

// class eResourceFlags {
//   static const int NOT_REMOVABLE = 1;
//   static const int NOT_CHANGED = 2; //you a get a copy Resource
// }

// class ResourcesContainer {
//   ResourcesContainer(this.res, this.flag);
//   int flag;
//   dynamic res;
// }

abstract class IResources {
  Map<String, dynamic> resources;

  IResources copy();
  ///Находит совпадение в [this] and [res] и на каждое совпадение вызывает [func]
  ///[func] должна вернуть либо новый обьект для [this][key]
  ///или null чтобы остановить поиск совпадений
  void coincidence(IResources res, dynamic Function(String key, dynamic obj1, dynamic obj2) func);

  ///Находит не совпадение в [this] and [res] и на каждое не совпадение вызывает [func]
  ///[func] должна вернуть true если продолжать искать несовпадение или false если прекратить
  ///if this contains(key) and res not [fromThis]=this[key] [fromRes] = null
  ///if res contains(key) and this not [fromThis]=null [fromRes] = res[key]
  void difference(IResources res, bool Function(String key, dynamic fromThis, dynamic fromRes) func);

  /// remove all [res][key] from this
  void sub(IResources res);

  ///add all element [res][key] to this. If [overwrite]==true overwrite [this][key]. 
  ///If [overwrite]==true, [coincidence] not call
  ///if [coincidence] !=null and [overwrite]==false, then this and res contains [key] [coincidence] execute 
  ///[coincidence] mast return new data to this[key].
  void add(IResources res, {bool overwrite = false,dynamic Function(String key, dynamic obj1, dynamic obj2) coincidence});

  ///Убирает ресурс [name] из ресурсов
  bool removeResource(String name);

  ///Доавляет ресурс[name] =[res] [resourceFlags] может быть 0|NOT_CHANGED|NOT_REMOVABLE
  bool addResource(String name, dynamic res);

  ///Проверка содержтся ли ресурс[name] в ресурсах
  bool contains(String name);

  ///Доступ к сущ. ресурсу[name]
  dynamic operator [](String name);

  ///Добавление нового ресурса[name] c flag =0 or change res if flag!=NOT_REMOVABLE
  operator []=(String name, dynamic res);
}

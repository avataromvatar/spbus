part of SimpleBuses;

///### SerialBus
///---
///Это шина в которой все узлы соединены последовательно.
///
///Потоки данных [TransportObject] могут идти как вверх[upStream] по узлам,
///так и вниз[downStream]
///
///Обработка каждого пакета данных осуществляется посредством определенных
///правил, представленных [IProcessingRules].
///
///По умолчания узлы не передают данные ниже или выше если нет определенного правила
///по этим данным
///Для того чтобы узел пропускал данные для которых у него нет правил, необходимо 
///в соответствующем [IPort] установить [unknownTopicSendFurther] = true     
abstract class ISerialBus {
  // Map<String, IPort<dynamic>> upStream;
  // Map<String, IPort<dynamic>> downStream;
  // List<ISerialBus> _busConnect;

  IPort get upStream;
  IPort get downStream;

  bool addRulesToUpStream<T>(String topic, IProcessingRules<T> rule);
  bool addRulesToDownStream<T>(String topic, IProcessingRules<T> rule);
  bool removeRulesFromUpStream<T>(String topic, IProcessingRules<T> rule);
  bool removeRulesFromDownStream<T>(String topic, IProcessingRules<T> rule);
  ///Соединяет узлы
  void connect(ISerialBus bus);
  ///Отсоединяет узлы
  void disconnect(ISerialBus bus);
  ///Изменяет путь к шине
  void changePath(String newPath);
  ///Убераем себя из цепочки связей и соединяем нижние узлы с верхними
  void softRemove();
  ///disconnect all connections
  void hardRemove();


  Future<TransportObject<dynamic>> sendUPandWaitEnd(String topic,dynamic obj);
  Future<TransportObject<dynamic>> sendDownAndWaitEnd(String topic,dynamic obj);

  bool sendUP<T>(TransportObject<T> obj);
  bool sendDown<T>(TransportObject<T> obj);
  ///Устанавливает максимальный счетчик для транспарта обьектов
  ///как защита от зацикливания одного и тогоже обьекта
  void setMaxCountToTransportObjeect(int max);

  ///create clone this SerialBus with all conections 
  ISerialBus clone({String name,String path,bool unknownTopicActionClone = true});
  ///copy all conection in [sbus]
  void copyTo(ISerialBus sbus);
}

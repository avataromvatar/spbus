part of SimpleBuses;

class SerialBus implements ISerialBus {
  IPort _upStream;
  IPort _downStream;

  IPort get upStream => _upStream;
  IPort get downStream => _downStream;
  String _path;
  String _name;
  String get path =>_path;
  bool _isInit = false;
  Map<int ,Completer<TransportObject<dynamic>>> _compliters;

  SerialBus(String name,{String path = "/",bool debugOn=false,bool unknownTopicSendFurther=true, int maxTransportObjectCOUNT=100}) {
    _upStream = Port("$name/upstream",path:path,unknownTopicSendFurther: unknownTopicSendFurther,maxTransportObjectCOUNT: maxTransportObjectCOUNT,debugOn: debugOn);
    _downStream = Port("$name/downstream",path:path,unknownTopicSendFurther: unknownTopicSendFurther,maxTransportObjectCOUNT: maxTransportObjectCOUNT,debugOn: debugOn);
  //  _upStream.debugOn = debugOn;
  //  _downStream.debugOn = debugOn;
    _name = name;
     _path = "$path$name";
     _compliters =   Map<int ,Completer<TransportObject<dynamic>>>();
  }
  SerialBus.clone(SerialBus sbus,{String name,String path,bool unknownTopicActionClone = true,bool cloneRules=true}) {
    String str=sbus._name;
    if(name!=null)
      str=name;
     _name = str;
    _upStream = Port.clone("$name/upstream",sbus.upStream,path:path!=null?path:sbus._path, unknownTopicActionClone: unknownTopicActionClone,cloneRules:cloneRules);
    _downStream = Port.clone("$name/downstream",sbus.downStream,path:path!=null?path:sbus._path,unknownTopicActionClone: unknownTopicActionClone,cloneRules:cloneRules);
    
    if(path!=null)
    _path = "$path$_name";
    else
    _path = "${sbus.path}$_name";
  }

  ISerialBus clone({String name,String path,bool unknownTopicActionClone = true})
  {
    return SerialBus.clone(this,path:path!=null?path:this._path,  name:name!=null?name:_name,unknownTopicActionClone: unknownTopicActionClone);
  }

  void changePath(String newPath)
  {
    _path = "$newPath$_name";
    _upStream.changePath(newPath);
    _downStream.changePath(newPath);
  }

  void softRemove()
  {
    _downStream.softRemove();
    _upStream.softRemove();
  }
   void setMaxCountToTransportObjeect(int max)
   {
     downStream.maxTransportObjectCOUNT = max;
     upStream.maxTransportObjectCOUNT=max;
   }


  bool addRulesToUpStream<T>(String topic, IProcessingRules<T> rule) {
    upStream.addRule(topic, rule);
  }

  bool addRulesToDownStream<T>(String topic, IProcessingRules<T> rule) {
    downStream.addRule(topic, rule);
  }

  bool removeRulesFromUpStream<T>(String topic, IProcessingRules<T> rule) {
    upStream.removeRule(topic, rules:rule);
  }

  bool removeRulesFromDownStream<T>(String topic, IProcessingRules<T> rule) {
    downStream.removeRule(topic, rules:rule);
  }

  void connect(ISerialBus bus) {
    upStream.connect(bus.upStream);
    bus.downStream.connect(downStream);
  }

  void disconnect(ISerialBus bus) {
    upStream.disconnect(bus.upStream);
    bus.downStream.disconnect(downStream);
  }
  // void setDefaultRules(IProcessingRules<dynamic> rule);

  Future<TransportObject<dynamic>> sendUPandWaitEnd(String topic,dynamic obj)
  {
    final Completer<TransportObject<dynamic>> cmp = Completer();

       _compliters[cmp.hashCode] = cmp;
    sendUP(TransportObject(topic, obj, 0, path,
        inTheEnd: (TransportObject objEnd, int codeOfEnd,
            {String path, int cloneCount}) {
       if(cloneCount ==0 || codeOfEnd == TransportObject.CODE_COMPLETE_BECAUSE_DONE)
       {
         cmp.complete(objEnd);
         _compliters.remove(cmp.hashCode);
       }
    }, identificator: cmp.hashCode));
    return cmp.future;
  }
  Future<TransportObject<dynamic>> sendDownAndWaitEnd(String topic,dynamic obj)
  {
final Completer<TransportObject<dynamic>> cmp = Completer();

       _compliters[cmp.hashCode] = cmp;
    sendDown(TransportObject(topic, obj, 0, path,
        inTheEnd: (TransportObject objEnd, int codeOfEnd,
            {String path, int cloneCount}) {
       if(cloneCount ==0 || codeOfEnd == TransportObject.CODE_COMPLETE_BECAUSE_DONE)
       {
         cmp.complete(objEnd);
         _compliters.remove(cmp.hashCode);
       }
    }, identificator: cmp.hashCode));
    return cmp.future;
  }

  bool sendUP<T>(TransportObject<T> obj) {
    return upStream.send<T>(obj, sender: upStream, senderName: upStream.name);
  }

  bool sendDown<T>(TransportObject<T> obj) {
    return downStream.send<T>(obj, sender: downStream, senderName: downStream.name);
  }

  void hardRemove()
  {
    _downStream.disconnectAll();
    _upStream.disconnectAll();
  }
  
 
  void copyTo(ISerialBus sbus)
  {
    _downStream.copyTo(sbus.downStream);
    _upStream.copyTo(sbus.upStream);
  }
}

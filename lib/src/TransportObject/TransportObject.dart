part of SimpleBuses;

///### TransportObject
///---
///Базовый класс для передачи данных по соответствующим топикам
class TransportObject<OBJECT> {
  final String topic;

  final String creatorName;

  ///transport object
  OBJECT obj;

  ///state of object
  int state = 0;
  int count = 0;

  // ///If true transmit this obj stop
  // bool isBroken = false;

  ///If true transmit this obj stop
  bool isEnded = false;

  ///if this object clone from origin
  bool isClone = false;

  static const int CODE_COMPLETE_IS_BROKEN = 1;
  static const int CODE_COMPLETE_NO_RECEIVERS = 2;
  static const int CODE_COMPLETE_BECAUSE_RECURSIVE = 3;
  static const int CODE_COMPLETE_BECAUSE_I_WANT = 4;
  static const int CODE_COMPLETE_BECAUSE_UNKNOWN_TOPIC = 5;

  ///был разделен на несколько потоков клонированием
  static const int CODE_COMPLETE_BECAUSE_SHARED = 6;

  ///Когда задача выполнена (только для целевых сообщений)
  static const int CODE_COMPLETE_BECAUSE_DONE = 7;

  ///слетчие клонов
  _CloneTransportObjectCount _cloneCount;
  int get countClone => _cloneCount.countClone;
  ///индентификатор обьекта
  final int identificator;

  ///Будет вызван если дальнейшее существование не возможно (нет приемников и тд )
  ///@attentioin вызывается для каждого клона, Если нужно вызвать только последний установите флаг [waitExactCompletion]
  final void Function(TransportObject<OBJECT> obj, int codeOfEnd, {String path, int cloneCount}) inTheEnd;

  ///Будет будет вызван если обьект клонирован
  final void Function(TransportObject<OBJECT> source, TransportObject<OBJECT> clone, {String path, String to, int cloneCount}) whenCloned;

  ///Если 1 то inTheEnd будет вызван при 0 счетчике клонов
  final bool waitExactCompletion;
  TransportObject(this.topic, this.obj, this.state, this.creatorName,
      {this.inTheEnd, this.identificator = 0, this.whenCloned, this.waitExactCompletion = false}) {
    _cloneCount = _CloneTransportObjectCount(0);
  }

  TransportObject.clone(TransportObject<OBJECT> obj, {String path, String to})
      : topic = obj.topic, 
        identificator = obj.identificator,
        inTheEnd = obj.inTheEnd,
        creatorName = obj.creatorName,
        whenCloned = obj.whenCloned,
        waitExactCompletion = obj.waitExactCompletion {
    _cloneCount = obj._cloneCount;
    _cloneCount.countClone++;
    isClone = true;
    this.obj = obj.obj;
    state = obj.state;
    if (whenCloned != null) whenCloned(obj, this, path: path, to: to);
  }
  TransportObject<OBJECT> clone({String path, String to}) {
    return TransportObject.clone(this, path: path, to: to);
  }

  bool newBegin(int newState) {
    if (isClone) _cloneCount.countClone++;
    isEnded = false;
    this.state = newState;
    count = 0;
    return true;
  }

  void itsShared({String path}) {
    isEnded = true;
    _reduceClone();
    if (inTheEnd != null) if ((waitExactCompletion && _cloneCount.countClone == 0) || (!waitExactCompletion))
      inTheEnd(this, CODE_COMPLETE_BECAUSE_SHARED, path: path, cloneCount: _cloneCount.countClone);
  }

  void itsBroken({String path}) {
    isEnded = true;
    _reduceClone();
    if (inTheEnd != null) if ((waitExactCompletion && _cloneCount.countClone == 0) || (!waitExactCompletion))
      inTheEnd(this, CODE_COMPLETE_IS_BROKEN, path: path, cloneCount: _cloneCount.countClone);
  }

  void itsNoHaveReceivers({String path}) {
    isEnded = true;
    _reduceClone();
    if (inTheEnd != null) if ((waitExactCompletion && _cloneCount.countClone == 0) || (!waitExactCompletion))
      inTheEnd(this, CODE_COMPLETE_NO_RECEIVERS, path: path, cloneCount: _cloneCount.countClone);
  }

  void itsEnd({String path}) {
    isEnded = true;
    _reduceClone();
    if (inTheEnd != null) if ((waitExactCompletion && _cloneCount.countClone == 0) || (!waitExactCompletion))
      inTheEnd(this, CODE_COMPLETE_BECAUSE_I_WANT, path: path, cloneCount: _cloneCount.countClone);
  }

  void itsRecursive({String path}) {
    isEnded = true;
    _reduceClone();
    if (inTheEnd != null) if ((waitExactCompletion && _cloneCount.countClone == 0) || (!waitExactCompletion))
      inTheEnd(this, CODE_COMPLETE_BECAUSE_RECURSIVE, path: path, cloneCount: _cloneCount.countClone);
  }

  void itsUnknown({String path}) {
    isEnded = true;
    _reduceClone();
    if (inTheEnd != null) if ((waitExactCompletion && _cloneCount.countClone == 0) || (!waitExactCompletion))
      inTheEnd(this, CODE_COMPLETE_BECAUSE_UNKNOWN_TOPIC, path: path, cloneCount: _cloneCount.countClone);
  }

  void itsDone({String path}) {
    isEnded = true;
    _reduceClone();
    if (inTheEnd != null) if ((waitExactCompletion && _cloneCount.countClone == 0) || (!waitExactCompletion))
      inTheEnd(this, CODE_COMPLETE_BECAUSE_DONE, path: path, cloneCount: _cloneCount.countClone);
  }

  void _reduceClone() {
    if (isClone) _cloneCount.countClone--;
  }
}

class _CloneTransportObjectCount {
  int countClone = 0;
  _CloneTransportObjectCount(this.countClone);
}

# Change Log for SimpleBuses


## [v0.6.0](https://bitbucket.org/gamemudteam/simplebus/commits/8ea8c61dd976496c35fec79c6b50dc7f068f0a56) - 2020-07-17

### Docs
* none :(

### Feature
* ParalelBus - send event to topic  
* SerialBus - have 2 channel upstream and downstream. send event up or down and execute chain rules for event
* SimpleBus - extend ParallelBus SerialBus 
* Resources - class conteiner data with protect from write or remove in basic method
* Listener - for parallel bus, subscrabe to topic and wait event
* Core - extend SimpleBus and Resources
* Port - main low class for SimpleBus
* ProcessingRules - execute rule for processing event
### NEED_TODO
* TODO add method [add, sub] to Resources

### Fixes
* none

### Bugs
* none

### Refactor
* none

## [v0.0.1](https://bitbucket.org/gamemudteam/simplebus/commits/85dbbb81f650ab34924b5bd6f43961ca0da40bdb) 

### NEED_TODO
* TODO ALL

